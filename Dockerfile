FROM microsoft/dotnet:2.2-sdk-alpine AS build
COPY . /src
WORKDIR /src/DockerDemo
RUN dotnet restore
RUN dotnet publish -c Release

FROM microsoft/dotnet:2.2-aspnetcore-runtime-alpine AS runtime
COPY --from=build /src/DockerDemo/bin/Release/netcoreapp2.2/publish /app
WORKDIR /app
ENTRYPOINT ["dotnet", "DockerDemo.dll"]
