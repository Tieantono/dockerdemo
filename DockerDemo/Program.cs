﻿using Dapper;
using System;
using System.Linq;

namespace DockerDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //var connectionString = Environment.GetEnvironmentVariable("DB");
            //using (var db = new Npgsql.NpgsqlConnection(connectionString))
            //{
            //    var version = db.Query<string>("SELECT VERSION()").First();
            //    Console.WriteLine(version);

            //    while (true)
            //    {
            //        // do not exit
            //    }
            //}

            var binary = new byte[256];

            var blob = new Blob
            {
                BlobId = Guid.NewGuid(),
                Name = "myfile.json",
                ContentType = "application/json"
            };

            if (System.IO.Directory.Exists("/data") == false)
            {
                System.IO.Directory.CreateDirectory("/data");
            }

            System.IO.File.WriteAllBytesAsync("/data/" + blob.BlobId, binary);
        }
    }

    internal class Blob
    {
        public Guid BlobId { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
    }
}
